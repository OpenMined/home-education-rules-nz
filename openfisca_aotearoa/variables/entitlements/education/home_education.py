# -*- coding: utf-8 -*-

# Import from openfisca-core the common python objects used to code the legislation in OpenFisca
from openfisca_core.model_api import *
# Import the entities specifically defined for this tax and benefit system
from openfisca_aotearoa.entities import Person, Family
from numpy import datetime64


class education__home_education_supervision_allowance_entitlement(Variable):
    value_type = float
    entity = Family
    definition_period = MONTH
    label = u'Value of a families entitlement for home education supervision allowance'
    reference = "https://parents.education.govt.nz/primary-school/schooling-in-nz/home-education/#canigetanyfinancialsupport"

    def formula(families, period, parameters):
        
        # identify number of children eligible (between the ages of six and sixteen)
        children_eligible = ((families.members("age", period) >= 6) & (families.members("age", period) <= 16))

        # there is an additional amount of eligibility for the first three children
        additional_based_on_children = (families.sum(children_eligible) > 0) * 371
        additional_based_on_children = additional_based_on_children + ((families.sum(children_eligible) > 1) * 260)
        additional_based_on_children = additional_based_on_children + ((families.sum(children_eligible) > 2) * 149)

        # calculate and return the family entitlement
        return families.sum(children_eligible * 372) + additional_based_on_children